﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Platf : MonoBehaviour {

    [SerializeField] private float obj_speed = 1;
    [SerializeField] private float reset_position = -25.1f;
    [SerializeField] private float start_position = 105.07f;

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	protected virtual void Update () {
        if (!GameManage.instance.GameOver)
        {
            transform.position+=(Vector3.left * (obj_speed * Time.deltaTime));

            if (transform.localPosition.x <= reset_position)
            {
                Vector3 newpos = new Vector3(start_position, transform.position.y, transform.position.z);
                transform.position = newpos;
            }
        }
       
    }
}
