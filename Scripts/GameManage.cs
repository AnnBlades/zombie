﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;

public class GameManage : MonoBehaviour {

    [SerializeField] private GameObject mainMenu;
    [SerializeField] private GameObject gameOverScreen;
    [SerializeField] private GameObject player;
    public static GameManage instance=null;

    void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else if (instance != this)
        {
            Destroy(gameObject);
        }
        DontDestroyOnLoad(gameObject);
        Assert.IsNotNull(mainMenu);
    }

    private bool playerActive = false;
    private bool gameOver = false;
    private bool gameStarted = false;

    public bool PlayerActive
    {
        get { return playerActive; }
    }
    public bool GameOver
    {
        get { return gameOver; }
    }
    public bool GameStarted
    {
        get { return gameStarted; }
    }

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void PlayerCollided()
    {
        gameOver = true;
        gameOverScreen.SetActive(true);
        playerActive = false;

    }
    public void PlayerStartedGame()
    {
        playerActive = true;
    }
    public void EnterGame()
    {
        gameOver = false;
        mainMenu.SetActive(false);
        //gameOverScreen.SetActive(false);
        gameStarted = true;
    }
    public void Replay()
    {
        gameOverScreen.SetActive(false);
        gameOver = false;
        gameStarted = true;
    }
    public void MainMenu()
    {
        gameOverScreen.SetActive(false);
        mainMenu.SetActive(true);
    }
}
