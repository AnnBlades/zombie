﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Respawn : MonoBehaviour {

    [SerializeField] private GameObject platrorm1;
    [SerializeField] private GameObject platrorm2;
    [SerializeField] private GameObject platrorm3;

    [SerializeField] private GameObject rock1;
    [SerializeField] private GameObject rock2;
    [SerializeField] private GameObject rock3;



    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {

        if(GameManage.instance.GameOver)
        {
            platrorm1.transform.position = new Vector3(16.7f, 3.91f, -3.6f);
            platrorm2.transform.position = new Vector3(60.4f, 3.91f, -3.6f);
            platrorm3.transform.position = new Vector3(104.04f, 3.91f, -3.6f);

            rock1.transform.position = new Vector3(8.06f, 4.2f, -2.4f);
            rock2.transform.position = new Vector3(16.17f, 3.08f, -2.4f);
            rock3.transform.position = new Vector3(22.73f, 1.4f, -2.4f);


        }

    }
}
