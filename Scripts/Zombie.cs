﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;

public class Zombie : MonoBehaviour
{
    private bool jump = false;
    [SerializeField] private float jump_force;
    private Rigidbody rigidBody;
    private Animator anim;
    [SerializeField] private AudioClip sfxJump;
    [SerializeField] private AudioClip sfxDeath;

    private AudioSource audio_Source;

    private void Awake()
    {
        Assert.IsNotNull(sfxJump);
        Assert.IsNotNull(sfxDeath);

    }

    // Use this for initialization
    void Start()
    {
        anim = GetComponent<Animator>();
        rigidBody = GetComponent<Rigidbody>();
        audio_Source = GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void Update()
    {
        if (!GameManage.instance.GameOver && GameManage.instance.GameStarted)
            if (Input.GetMouseButtonDown(0))
            {
                GameManage.instance.PlayerStartedGame();
                anim.Play("Jump");
                jump = true;
                rigidBody.useGravity = true;
                rigidBody.isKinematic = false;
                rigidBody.detectCollisions = true;
                audio_Source.PlayOneShot(sfxJump);
            }
        if(GameManage.instance.GameOver)
        {
            rigidBody.useGravity = false;
            rigidBody.isKinematic = true;
            rigidBody.detectCollisions = false;
            transform.position = new Vector3(-0.01722878f, 2.5f, -2.41283f);
            
        }

    }
    private void FixedUpdate()
    {
        if (jump == true)
        {
            jump = false;
            rigidBody.velocity = new Vector2(0, 0);
            rigidBody.AddForce(new Vector2(0, jump_force), ForceMode.Impulse);
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag == "obstance")
        {
            rigidBody.AddForce(new Vector2(-50, 20), ForceMode.Impulse);
            audio_Source.PlayOneShot(sfxDeath);
            rigidBody.detectCollisions = false;
            GameManage.instance.PlayerCollided();

        }
    }

    
}
