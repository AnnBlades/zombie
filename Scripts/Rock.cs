﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Rock : Platf
{

    [SerializeField] Vector3 top_pos;
    [SerializeField] Vector3 bottom_pos;
    [SerializeField] float speed;

    Vector3 rock_center;

    // Use this for initialization
    void Start()
    {
        StartCoroutine(Move(bottom_pos));

    }


    protected override void Update()
    {
        if (GameManage.instance.PlayerActive)
        {
            base.Update();
            transform.Rotate(Vector3.up, -100 * -Time.deltaTime);
        }

    }
    IEnumerator Move(Vector3 target)
    {
        transform.Rotate(Vector3.up, 20 * Time.deltaTime);

        while (Mathf.Abs((target - transform.localPosition).y) > 0.20f)
        {

            Vector3 direction = target.y == top_pos.y ? Vector3.up : Vector3.down;
            transform.localPosition += direction * Time.deltaTime * speed;
            
            yield return null;
        }
        yield return new WaitForSeconds(0.5f);
        Vector3 newtarget = target.y == top_pos.y ? bottom_pos : top_pos;
        StartCoroutine(Move(newtarget));

    }

}
